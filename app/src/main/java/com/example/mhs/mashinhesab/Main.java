/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.mhs.mashinhesab;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author HP
 */
public class Main {

    List<Variable> vars = new ArrayList<Variable>();
    List<Function> funcs = new ArrayList<Function>();
    Scanner sc = new Scanner(System.in);
    boolean err = false;

    public Main() {
        



        //*******************
        funcs.add(new Function("not", "(-1)*x"));
        funcs.add(new Function("sin", "x"));
        funcs.add(new Function("cos", "x"));
        funcs.add(new Function("tan", "x"));
        funcs.add(new Function("cot", "x"));
        funcs.add(new Function("arctan", "x"));
        funcs.add(new Function("arccot", "x"));
        funcs.add(new Function("sign", "x"));
        funcs.add(new Function("absolute", "x"));
//        funcs.add(new Function("ex", "x"));
//        funcs.add(new Function("x2", "x"));
//        funcs.add(new Function("x3", "x"));
//        funcs.add(new Function("xy", "x"));
//        funcs.add(new Function("^", "x"));
//        funcs.add(new Function("%", "x"));

        vars.add(new Variable(Math.PI, "pi"));
        vars.add(new Variable(Math.E, "e"));
        // process("2*sin(pi)+absolute(2)+sign(-19)*cos(0)+tan(0)");

       // get();

    }

//    private void get() {// get and process input string
//        String s = sc.nextLine();
//        s = s.toLowerCase();
//        s = s.replaceAll(" ", "");
//        if (s.equals("exit") || s.equals("")) {
//            System.exit(0);
//        }
//        if (s.contains("var") || s.contains("function")) {
//            if (s.substring(0, 3).equals("var")) {
//                //System.out.println(s.substring(0, 3).equals("var"));
//                getVar(s);
//                get();
//            } else if (s.substring(0, 8).equals("function")) {
//                getFunc(s);
//                get();
//            }
//
//        } else {
//            double d = solve(s);
//            System.out.println("= " + d);
//            get();
//        }
//
//
//    }

    public double findVariable(String name) {
        err = false;
        for (int i = 0; i < vars.size(); i++) {
            if (vars.get(i).getName().equals(name)) {
                return vars.get(i).getValue();
            }
        }
        err = true;
        return 0;

    }

    public double findFunction(String name, double value) {
        err = false;
        for (int i = 0; i < funcs.size(); i++) {
            if (funcs.get(i).getName().equals(name)) {
                String s = funcs.get(i).getFunction();
                s = s.replaceAll("x", "" + value);
                return solve(s);
            }
        }
        err = true;
        return 0;

    }

    public void getVar(String s) {//declar the variable
        String name = s.substring(3, s.indexOf("="));
        double value = findFunction(name, 1);
        if(!err){
            System.err.println(name + " is a function");
//            get();
        }
        value = findVariable(name);
        if (!err) {
            System.err.println("Variable " + name + " Alredy declared");
        } else {
            value = solve(s.substring(s.indexOf('=') + 1, s.length()));
            value = round(value);
            vars.add(new Variable(value, name));
            System.err.println(name + " = " + value);
            /* if (!err) {
            vars.add(new Variable(value, name));//!!!!!!!!!!!!!!!!!!!!
            } else {
            System.err.println("Wrong input!");
            }*/


        }
    }

    private double round(double d) {
        if (Math.abs(d) > 9999999999.0) {
            System.err.println("Too Long");
//            get();
        } else if (Math.abs(d) < 0.000000001) {
            return 0;

        }

        return d;

    }

    public double calc(double left, double right, String op) {
        // System.out.println("( " + left + " " + op + " " + right + " )");
        char operator = op.charAt(0);
        switch (operator) {
            case '+':
                return round(left + right);
            case '-':
                return round(left - right);
            case '*':
                return round(left * right);
            case '/':
                return round(left / right);
            case '^':
                return round(Math.pow(left,right));
            case '%':
                return round(left%right);

            default:
            //System.out.println("calulation error ( " + left + " " + operator + " " + right + " )");

        }
        if (op.equals("not")) {
            return round(right * (-1));
        } else if (op.equals("sin")) {
            return round(Math.sin(right));
        } else if (op.equals("cos")) {
            return round(Math.cos(right));
        } else if (op.equals("tan")) {
            return round(Math.tan(right));
        } else if (op.equals("cot")) {
            return round(Math.cos(right) / Math.sin(right));
        } else if (op.equals("absolute")) {
            return round(Math.abs(right));
        } else if (op.equals("sign")) {
            return round(Math.signum(right));
        } else if (op.equals("arcsin")) {
            return round(Math.asin(right));
        } else if (op.equals("arccos")) {
            return round(Math.acos(right));
        } else if (op.equals("arctan")) {
            return round(Math.atan(right));
        } else if (op.equals("arccot")) {
            return round(Math.asin(right));//====================
        }
        double d = findFunction(op, right);
        if (!err) {
            return d;
        } else//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        {
            System.err.println("Wrong input!");
        }




        return 0;




    }

    public void getFunc(String s) {
        String name = s.substring(8, s.indexOf("="));
        String func = s.substring(s.indexOf('=') + 1, s.length());
        boolean flag = false;
        double d = findVariable(name);
        if(!err){
            System.err.println(name + " is a variable");
//            get();
        }
        for(int i=0;i<funcs.size();i++)
            if(funcs.get(i).getName().equals(name))
                flag = true;
        if(flag)
            System.err.println("Function " + name + " Alredy declared");
        else {
            String buff = func.replaceAll("x", "1");
            double dd = solve(buff);
            funcs.add(new Function(name, func));
        }


    }

    public double solve(String s) {
        int i = 0;
        List<String> input = new ArrayList<String>();
        while (i < s.length()) {

            if (isDouble(s.charAt(i) + "")) {
                int b = i;
                while (i < s.length() && isDouble(s.charAt(i) + "")) {
                    i++;
                }
                input.add(s.substring(b, i));
                i--;
            } else if (isOperator(s.charAt(i))) {// s[i] is (+ - * /) or negative
                if (s.charAt(i) == '-' && (i == 0 || (s.charAt(i - 1) != ')') && !isDouble(s.charAt(i - 1) + ""))) {// - is negative
                    input.add("not");
                } else {
                    input.add(s.charAt(i) + "");
                }

            } else if (s.charAt(i) == '(' || s.charAt(i) == ')') {
                input.add(s.charAt(i) + "");
            } else {
                int b = i;
                while (i < s.length() && !isOperator(s.charAt(i)) && s.charAt(i) != '(' && s.charAt(i) != ')') {
                    i++;
                }
                double d = findVariable(s.substring(b, i));
                if (!err) {
                    if (d < 0) {
                        input.add("not");
                        d = d * (-1);
                    }

                    input.add(d + "");

                } else {
                    d = findFunction(s.substring(b, i), 1);
                    if (!err) {
                        input.add(s.substring(b, i));
                    } else {
                        if (i < s.length() && s.charAt(i) == '(') {
                            System.err.println("Function " + s.substring(b, i) + " not declared");
                        } else {
                            System.err.println("Variable " + s.substring(b, i) + " not declared");
                        }
                       // get();
                    }

                }
                i--;
            }

            i++;
        }
        /* for(i = 0;i<input.size();i++){
        System.out.println(i+" : "+input.get(i));
        }*/
        i = 0;
        Stack<Double> operand = new Stack<Double>();
        Stack<String> operator = new Stack<String>();
        try {
            while (i < input.size()) {
                if (isDouble(input.get(i))) {//input[i] is aa number
                    operand.push(Double.valueOf(input.get(i)));
                } else if (isOperator(input.get(i).charAt(0))) {//input[i] is (+ - * /)
                    if (canPush(input.get(i).charAt(0), operator)) {
                        operator.push(input.get(i));
                    } else {
                        double d = operand.pop();
                        while (!operator.empty() && !canPush(input.get(i).charAt(0), operator)) {
                            if (isOperator(operator.peek().charAt(0))) {
                                d = calc(operand.pop(), d, operator.pop());
                            } else {
                                d = calc(0, d, operator.pop());
                            }
                        }
                        operand.push(d);
                        operator.push(input.get(i));
                    }
                } else if (input.get(i).equals(")")) {
                    double d = operand.pop();
                    while (!operator.empty() && !operator.peek().equals("(")) {
                        if (isOperator(operator.peek().charAt(0))) {
                            d = calc(operand.pop(), d, operator.pop());
                        } else {
                            d = calc(0, d, operator.pop());
                        }
                    }
                    operand.push(d);
                    if (!operator.empty() && operator.peek().equals("(")) {
                        operator.pop();
                    } else {
                        // System.err.println("Wrong input!  2" + operator.empty());//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
                    }
                } else { // input[i] is a function
                    operator.push(input.get(i));
                }

                i++;
            }

            double d = operand.pop();
            while (!operator.empty() && !operator.empty()) {
                if (isOperator(operator.peek().charAt(0))) {
                    d = calc(operand.pop(), d, operator.pop());
                } else {
                    d = calc(0, d, operator.pop());
                }
            }

            if (operator.empty() && operand.empty()) {
                return d;
            } else {
                //System.err.println("Wrong input!  3");//!!!!!!!!!!!!!!!!!!!!!!
            }

        } catch (EmptyStackException t) {
            System.err.print("Wrong Input!");
//            get();
        }





        return 0;

    }

    public boolean canPush(char operator, Stack<String> ops) {
        if (ops.isEmpty()) {
            return true;
        }
        if (isOperator(ops.peek().charAt(0))) {
            if (getOrder(operator) <= getOrder(ops.peek().charAt(0))) {
                return false;
            }
        } else {
            double d = findFunction(ops.peek(), 1);
            if (!err) {
                return false;
            }
        }



        return true;
    }

    public boolean isOperator(char c) {
        String s = "+ - * /";
        return s.contains(c + "");
    }

    public int getOrder(char c) {
        switch (c) {
            case '+':
            case '-':
                return 0;
            case '*':
            case '/':
                return 1;
        }
        return 0;
    }

    public boolean isDouble(String s) {
        for (int i = 0; i < s.length(); i++) {
            if ((s.charAt(i) >= '0' && s.charAt(i) <= '9') || s.charAt(i) == '.') {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        new Main();
    }
}
